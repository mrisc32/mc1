----------------------------------------------------------------------------------------------------
-- Copyright (c) 2019 Marcus Geelnard
--
-- This software is provided 'as-is', without any express or implied warranty. In no event will the
-- authors be held liable for any damages arising from the use of this software.
--
-- Permission is granted to anyone to use this software for any purpose, including commercial
-- applications, and to alter it and redistribute it freely, subject to the following restrictions:
--
--  1. The origin of this software must not be misrepresented; you must not claim that you wrote
--     the original software. If you use this software in a product, an acknowledgment in the
--     product documentation would be appreciated but is not required.
--
--  2. Altered source versions must be plainly marked as such, and must not be misrepresented as
--     being the original software.
--
--  3. This notice may not be removed or altered from any source distribution.
----------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.vid_types.all;

----------------------------------------------------------------------------------------------------
-- Video control registers.
----------------------------------------------------------------------------------------------------

entity vid_regs is
  port(
    i_rst : in std_logic;
    i_clk : in std_logic;

    i_restart_frame : in std_logic;
    i_write_enable : in std_logic;
    i_write_addr : in std_logic_vector(3 downto 0);
    i_write_data : in std_logic_vector(23 downto 0);

    o_regs : out T_VID_REGS
  );
end vid_regs;

architecture rtl of vid_regs is
  constant C_DEFAULT_ADDR : std_logic_vector(23 downto 0) := x"000000";
  constant C_DEFAULT_XOFFS : std_logic_vector(23 downto 0) := x"000000";
  constant C_DEFAULT_XINCR : std_logic_vector(23 downto 0) := x"004000";
  constant C_DEFAULT_HSTRT : std_logic_vector(23 downto 0) := x"000000";
  constant C_DEFAULT_HSTOP : std_logic_vector(23 downto 0) := x"000000";
  constant C_DEFAULT_CMODE : std_logic_vector(23 downto 0) := x"000002";
  constant C_DEFAULT_RMODE : std_logic_vector(23 downto 0) := x"000135";
  constant C_DEFAULT_SUBROW : std_logic_vector(23 downto 0) := x"000000";
  constant C_DEFAULT_TEXTBG : std_logic_vector(23 downto 0) := x"e10000";
  constant C_DEFAULT_TEXTFG : std_logic_vector(23 downto 0) := x"fff820";

  signal s_regs : T_VID_REGS;

  signal s_we_addr : std_logic;
  signal s_we_xoffs : std_logic;
  signal s_we_xincr : std_logic;
  signal s_we_hstrt : std_logic;
  signal s_we_hstop : std_logic;
  signal s_we_cmode : std_logic;
  signal s_we_rmode : std_logic;
  signal s_we_subrow : std_logic;
  signal s_we_textbg : std_logic;
  signal s_we_textfg : std_logic;
  signal s_write_data : std_logic_vector(23 downto 0);
begin
  -- Clocked registers. Writes are pipelined.
  process(i_clk, i_rst)
  begin
    if i_rst = '1' then
      s_regs.ADDR <= C_DEFAULT_ADDR;
      s_regs.XOFFS <= C_DEFAULT_XOFFS;
      s_regs.XINCR <= C_DEFAULT_XINCR;
      s_regs.HSTRT <= C_DEFAULT_HSTRT;
      s_regs.HSTOP <= C_DEFAULT_HSTOP;
      s_regs.CMODE <= C_DEFAULT_CMODE;
      s_regs.RMODE <= C_DEFAULT_RMODE;
      s_regs.SUBROW <= C_DEFAULT_SUBROW;
      s_regs.TEXTBG <= C_DEFAULT_TEXTBG;
      s_regs.TEXTFG <= C_DEFAULT_TEXTFG;

      s_we_addr <= '0';
      s_we_xoffs <= '0';
      s_we_xincr <= '0';
      s_we_hstrt <= '0';
      s_we_hstop <= '0';
      s_we_cmode <= '0';
      s_we_rmode <= '0';
      s_we_subrow <= '0';
      s_we_textbg <= '0';
      s_we_textfg <= '0';
      s_write_data <= (others => '0');
    elsif rising_edge(i_clk) then
      -- Stage 1: Detect which register is being written to (if any).
      if i_write_enable = '1' and i_write_addr = "0000" then
        s_we_addr <= '1';
      else
        s_we_addr <= '0';
      end if;
      if i_write_enable = '1' and i_write_addr = "0001" then
        s_we_xoffs <= '1';
      else
        s_we_xoffs <= '0';
      end if;
      if i_write_enable = '1' and i_write_addr = "0010" then
        s_we_xincr <= '1';
      else
        s_we_xincr <= '0';
      end if;
      if i_write_enable = '1' and i_write_addr = "0011" then
        s_we_hstrt <= '1';
      else
        s_we_hstrt <= '0';
      end if;
      if i_write_enable = '1' and i_write_addr = "0100" then
        s_we_hstop <= '1';
      else
        s_we_hstop <= '0';
      end if;
      if i_write_enable = '1' and i_write_addr = "0101" then
        s_we_cmode <= '1';
      else
        s_we_cmode <= '0';
      end if;
      if i_write_enable = '1' and i_write_addr = "0110" then
        s_we_rmode <= '1';
      else
        s_we_rmode <= '0';
      end if;
      if i_write_enable = '1' and i_write_addr = "0111" then
        s_we_subrow <= '1';
      else
        s_we_subrow <= '0';
      end if;
      if i_write_enable = '1' and i_write_addr = "1000" then
        s_we_textbg <= '1';
      else
        s_we_textbg <= '0';
      end if;
      if i_write_enable = '1' and i_write_addr = "1001" then
        s_we_textfg <= '1';
      else
        s_we_textfg <= '0';
      end if;
      s_write_data <= i_write_data;

      -- Stage 2: Perform write.
      if i_restart_frame = '1' then
        s_regs.ADDR <= C_DEFAULT_ADDR;
        s_regs.XOFFS <= C_DEFAULT_XOFFS;
        s_regs.XINCR <= C_DEFAULT_XINCR;
        s_regs.HSTRT <= C_DEFAULT_HSTRT;
        s_regs.HSTOP <= C_DEFAULT_HSTOP;
        s_regs.CMODE <= C_DEFAULT_CMODE;
        s_regs.RMODE <= C_DEFAULT_RMODE;
        s_regs.SUBROW <= C_DEFAULT_SUBROW;
        s_regs.TEXTBG <= C_DEFAULT_TEXTBG;
        s_regs.TEXTFG <= C_DEFAULT_TEXTFG;
      else
        if s_we_addr = '1' then
          s_regs.ADDR <= s_write_data;
        end if;
        if s_we_xoffs = '1' then
          s_regs.XOFFS <= s_write_data;
        end if;
        if s_we_xincr = '1' then
          s_regs.XINCR <= s_write_data;
        end if;
        if s_we_hstrt = '1' then
          s_regs.HSTRT <= s_write_data;
        end if;
        if s_we_hstop = '1' then
          s_regs.HSTOP <= s_write_data;
        end if;
        if s_we_cmode = '1' then
          s_regs.CMODE <= s_write_data;
        end if;
        if s_we_rmode = '1' then
          s_regs.RMODE <= s_write_data;
        end if;
        if s_we_subrow = '1' then
          s_regs.SUBROW <= s_write_data;
        end if;
        if s_we_textbg = '1' then
          s_regs.TEXTBG <= s_write_data;
        end if;
        if s_we_textfg = '1' then
          s_regs.TEXTFG <= s_write_data;
        end if;
      end if;
    end if;
  end process;

  -- Outputs.
  o_regs <= s_regs;
end rtl;
