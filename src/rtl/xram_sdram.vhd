----------------------------------------------------------------------------------------------------
-- Copyright (c) 2020 Marcus Geelnard
--
-- This software is provided 'as-is', without any express or implied warranty. In no event will the
-- authors be held liable for any damages arising from the use of this software.
--
-- Permission is granted to anyone to use this software for any purpose, including commercial
-- applications, and to alter it and redistribute it freely, subject to the following restrictions:
--
--  1. The origin of this software must not be misrepresented; you must not claim that you wrote
--     the original software. If you use this software in a product, an acknowledgment in the
--     product documentation would be appreciated but is not required.
--
--  2. Altered source versions must be plainly marked as such, and must not be misrepresented as
--     being the original software.
--
--  3. This notice may not be removed or altered from any source distribution.
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
-- This provides an intelligent Wishbone interface for SDRAM memories.
--
-- Features:
--  - Pipelined and can keep several active requests in a FIFO queue.
--  - A write combiner reduces the number of write operations (esp. for consecutive byte writes).
--
-- TODO(m): Add a simple caching mechanism, and transfer more than 32 bits per request.
----------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity xram_sdram is
  generic (
    -- Clock frequency (in Hz)
    CPU_CLK_HZ : integer;

    -- See sdram.vhd for the details of these generics.
    SDRAM_ADDR_WIDTH : integer := 13;
    SDRAM_DATA_WIDTH : integer := 16;
    SDRAM_COL_WIDTH : integer := 9;
    SDRAM_ROW_WIDTH : integer := 13;
    SDRAM_BANK_WIDTH : integer := 2;
    CAS_LATENCY : integer := 2;
    T_DESL : real := 200_000.0;
    T_MRD : real := 12.0;
    T_RC : real := 60.0;
    T_RCD : real := 18.0;
    T_RP : real := 18.0;
    T_DPL : real := 14.0;
    T_REF : real := 64_000_000.0;

    -- FIFO configuration.
    FIFO_DEPTH : integer := 16
  );
  port (
    -- Reset signal.
    i_rst : in std_logic;

    -- Wishbone memory interface (b4 pipelined slave).
    -- See: https://cdn.opencores.org/downloads/wbspec_b4.pdf
    i_wb_clk : in std_logic;
    i_wb_cyc : in std_logic;
    i_wb_stb : in std_logic;
    i_wb_adr : in std_logic_vector(28 downto 0);
    i_wb_dat : in std_logic_vector(63 downto 0);
    i_wb_we : in std_logic;
    i_wb_sel : in std_logic_vector(64/8-1 downto 0);
    o_wb_dat : out std_logic_vector(63 downto 0);
    o_wb_ack : out std_logic;
    o_wb_stall : out std_logic;
    o_wb_err : out std_logic;

    -- SDRAM interface.
    o_sdram_a : out std_logic_vector(SDRAM_ADDR_WIDTH-1 downto 0);
    o_sdram_ba : out std_logic_vector(SDRAM_BANK_WIDTH-1 downto 0);
    io_sdram_dq : inout std_logic_vector(SDRAM_DATA_WIDTH-1 downto 0);
    o_sdram_cke : out std_logic;
    o_sdram_cs_n : out std_logic;
    o_sdram_ras_n : out std_logic;
    o_sdram_cas_n : out std_logic;
    o_sdram_we_n : out std_logic;
    o_sdram_dqm : out std_logic_vector(SDRAM_DATA_WIDTH/8-1 downto 0)
  );
end xram_sdram;

architecture rtl of xram_sdram is
  constant C_ADDR_WIDTH : integer := SDRAM_COL_WIDTH+SDRAM_ROW_WIDTH+SDRAM_BANK_WIDTH-2;
  constant C_DATA_WIDTH : integer := i_wb_dat'length;
  constant C_SEL_WIDTH : integer := C_DATA_WIDTH/8;

  constant C_MEM_OP_WIDTH : integer := C_ADDR_WIDTH + C_DATA_WIDTH + C_SEL_WIDTH + 1;

  constant C_SEL_ALL : std_logic_vector((C_DATA_WIDTH/8)-1 downto 0) := std_logic_vector(to_signed(-1, (C_DATA_WIDTH/8)));

  -- FIFO signals.
  signal s_fifo_wr_en : std_logic;
  signal s_fifo_wr_data : std_logic_vector(C_MEM_OP_WIDTH-1 downto 0);
  signal s_fifo_full : std_logic;
  signal s_fifo_rd_en : std_logic;
  signal s_fifo_rd_data : std_logic_vector(C_MEM_OP_WIDTH-1 downto 0);
  signal s_fifo_empty : std_logic;

  -- Map of FIFO outputs.
  alias a_fifo_rd_adr : std_logic_vector(C_ADDR_WIDTH-1 downto 0) is s_fifo_rd_data(C_ADDR_WIDTH+C_DATA_WIDTH+C_SEL_WIDTH+1-1 downto C_DATA_WIDTH+C_SEL_WIDTH+1);
  alias a_fifo_rd_dat : std_logic_vector(C_DATA_WIDTH-1 downto 0) is s_fifo_rd_data(C_DATA_WIDTH+C_SEL_WIDTH+1-1 downto C_SEL_WIDTH+1);
  alias a_fifo_rd_sel : std_logic_vector(C_SEL_WIDTH-1 downto 0) is s_fifo_rd_data(C_SEL_WIDTH+1-1 downto 1);
  alias a_fifo_rd_we : std_logic is s_fifo_rd_data(0);

  -- Write combiner.
  signal s_wc_has_data : std_logic;
  signal s_next_wc_has_data : std_logic;
  signal s_wc_adr : std_logic_vector(C_ADDR_WIDTH-1 downto 0);
  signal s_next_wc_adr : std_logic_vector(C_ADDR_WIDTH-1 downto 0);
  signal s_wc_dat : std_logic_vector(C_DATA_WIDTH-1 downto 0);
  signal s_next_wc_dat : std_logic_vector(C_DATA_WIDTH-1 downto 0);
  signal s_wc_sel : std_logic_vector(C_SEL_WIDTH-1 downto 0);
  signal s_next_wc_sel : std_logic_vector(C_SEL_WIDTH-1 downto 0);

  -- ACK logic.
  subtype T_READ_COUNT is integer range 0 to 3;
  signal s_pending_reads : T_READ_COUNT;
  signal s_can_pop_writes : std_logic;

  -- SDRAM signals.
  signal s_sdram_adr : std_logic_vector(C_ADDR_WIDTH-1 downto 0);
  signal s_sdram_dat_w : std_logic_vector(C_DATA_WIDTH-1 downto 0);
  signal s_sdram_we : std_logic;
  signal s_sdram_sel : std_logic_vector(C_SEL_WIDTH-1 downto 0);
  signal s_sdram_req : std_logic;
  signal s_sdram_busy : std_logic;
  signal s_sdram_ack : std_logic;
  signal s_sdram_dat : std_logic_vector(C_DATA_WIDTH-1 downto 0);
begin
  --------------------------------------------------------------------------------------------------
  -- Wishbone request FIFO.
  --
  -- The main purposes of the FIFO is to add an extra pipeline stage to the memory interface, thus
  -- allowing for higher clocks, and also to allow several concurrent requests in a pipelined manner
  -- without blocking the Wishbone host.
  --------------------------------------------------------------------------------------------------

  -- Instantiate the memory operation FIFO.
  fifo_1: entity work.fifo
    generic map (
      G_WIDTH => C_MEM_OP_WIDTH,
      G_DEPTH => FIFO_DEPTH
    )
    port map (
      i_rst => i_rst,
      i_clk => i_wb_clk,
      i_wr_en => s_fifo_wr_en,
      i_wr_data => s_fifo_wr_data,
      o_full => s_fifo_full,
      i_rd_en => s_fifo_rd_en,
      o_rd_data => s_fifo_rd_data,
      o_empty => s_fifo_empty
    );

  -- Write to the FIFO.
  s_fifo_wr_en <= i_wb_cyc and i_wb_stb and not s_fifo_full;
  s_fifo_wr_data <= i_wb_adr(C_ADDR_WIDTH-1 downto 0) &
                    i_wb_dat &
                    i_wb_sel &
                    i_wb_we;


  --------------------------------------------------------------------------------------------------
  -- Write combiner.
  --
  -- The write combiner does what it sounds like: It combines several write operations into a single
  -- write operation, when possible. This is especially useful for sequential byte writes.
  --------------------------------------------------------------------------------------------------

  -- TODO(m): Bypass the WC when a_fifo_rd_sel = "11111111", even when it has data.
  -- TODO(m): Bypass the WC for read requests, even when it has data. Handle ACK reordering.

  process (all)
    variable v_idx : integer;
    variable v_sel : std_logic_vector(C_SEL_WIDTH-1 downto 0);
    variable v_dat : std_logic_vector(C_DATA_WIDTH-1 downto 0);
  begin
    -- Default FIFO read operation.
    s_fifo_rd_en <= '0';

    -- Default write combiner state.
    s_next_wc_has_data <= s_wc_has_data;
    s_next_wc_adr <= s_wc_adr;
    s_next_wc_dat <= s_wc_dat;
    s_next_wc_sel <= s_wc_sel;

    -- Default command to the SDRAM controller.
    s_sdram_req <= '0';
    s_sdram_adr <= a_fifo_rd_adr;
    s_sdram_dat_w <= a_fifo_rd_dat;
    s_sdram_sel <= a_fifo_rd_sel;
    s_sdram_we <= a_fifo_rd_we;

    if s_fifo_empty = '0' then
      if a_fifo_rd_we = '1' and s_can_pop_writes = '1' then
        -- Incoming write request.
        if s_wc_has_data = '0' then
          if a_fifo_rd_sel = C_SEL_ALL then
            -- Just let the write request through.
            if s_sdram_busy = '0' then
              s_fifo_rd_en <= '1';
              s_sdram_req <= '1';
            end if;
          else
            -- Initialize the write combiner buffer.
            s_next_wc_adr <= a_fifo_rd_adr;
            s_next_wc_dat <= a_fifo_rd_dat;
            s_next_wc_sel <= a_fifo_rd_sel;
            s_next_wc_has_data <= '1';
            s_fifo_rd_en <= '1';
          end if;
        else
          if s_wc_adr = a_fifo_rd_adr then
            -- Mix the new write request into the write combiner buffer.
            for k in 0 to (C_DATA_WIDTH/8)-1 loop
              v_idx := k * 8;
              if a_fifo_rd_sel(k) = '1' then
                v_dat(v_idx+7 downto v_idx) := a_fifo_rd_dat(v_idx+7 downto v_idx);
              else
                v_dat(v_idx+7 downto v_idx) := s_wc_dat(v_idx+7 downto v_idx);
              end if;
            end loop;
            v_sel := s_wc_sel or a_fifo_rd_sel;

            s_next_wc_dat <= v_dat;
            s_next_wc_sel <= v_sel;
            s_fifo_rd_en <= '1';

            -- Can we flush the data immediately?
            if v_sel = C_SEL_ALL and s_sdram_busy = '0' then
              s_next_wc_has_data <= '0';
              s_sdram_req <= '1';
              s_sdram_adr <= s_wc_adr;
              s_sdram_dat_w <= v_dat;
              s_sdram_sel <= v_sel;
              s_sdram_we <= '1';
            end if;
          else
            -- We have a write to a new address, so we need to flush the old combiner buffer content.
            if s_sdram_busy = '0' then
              -- Flush the old content of the combiner buffer.
              s_sdram_req <= '1';
              s_sdram_adr <= s_wc_adr;
              s_sdram_dat_w <= s_wc_dat;
              s_sdram_sel <= s_wc_sel;
              s_sdram_we <= '1';

              -- Replace the contents of the write combiner buffer with the new request.
              s_next_wc_adr <= a_fifo_rd_adr;
              s_next_wc_dat <= a_fifo_rd_dat;
              s_next_wc_sel <= a_fifo_rd_sel;
              s_fifo_rd_en <= '1';
            end if;
          end if;
        end if;
      elsif a_fifo_rd_we = '0' then
        -- Incoming read request.
        if s_wc_has_data = '0' then
          -- Just let the read request through.
          if s_sdram_busy = '0' then
            s_fifo_rd_en <= '1';
            s_sdram_req <= '1';
          end if;
        else
          -- We must flush the write combiner buffer before we can read.
          if s_sdram_busy = '0' then
            s_next_wc_has_data <= '0';
            s_sdram_req <= '1';
            s_sdram_adr <= s_wc_adr;
            s_sdram_dat_w <= s_wc_dat;
            s_sdram_sel <= s_wc_sel;
            s_sdram_we <= '1';
          end if;
        end if;
      end if;
    else
      -- No pending requests... Should we flush the write combiner buffer?
      if s_wc_has_data = '1' and s_wc_sel = C_SEL_ALL and s_sdram_busy = '0' then
        s_next_wc_has_data <= '0';
        s_sdram_req <= '1';
        s_sdram_adr <= s_wc_adr;
        s_sdram_dat_w <= s_wc_dat;
        s_sdram_sel <= s_wc_sel;
        s_sdram_we <= '1';
      end if;
    end if;
  end process;

  process (i_wb_clk, i_rst)
  begin
    if i_rst = '1' then
      s_wc_has_data <= '0';
      s_wc_adr <= (others => '0');
      s_wc_dat <= (others => '0');
      s_wc_sel <= (others => '0');
    elsif rising_edge(i_wb_clk) then
      s_wc_has_data <= s_next_wc_has_data;
      s_wc_adr <= s_next_wc_adr;
      s_wc_dat <= s_next_wc_dat;
      s_wc_sel <= s_next_wc_sel;
    end if;
  end process;


  --------------------------------------------------------------------------------------------------
  -- ACK tracking.
  --------------------------------------------------------------------------------------------------

  -- We ACK back to the Wishbone host as soon as we:
  --   - Pop a write request from the FIFO
  --   - ...or we get a read ACK from the SDRAM
  -- Note that these are (and must be) mutually exclusive events.
  --
  -- TODO(m): Add an extra register for the response signals - i.e. pipeline them for better timing?
  o_wb_ack <= '1' when (s_fifo_rd_en = '1' and a_fifo_rd_we = '1') or
                       (s_pending_reads /= 0 and s_sdram_ack = '1') else '0';
  o_wb_dat <= s_sdram_dat;

  -- Additional Wishbone outputs.
  o_wb_stall <= s_fifo_full;
  o_wb_err <= '0';

  -- Keep track of pending read operations.
  process (i_wb_clk, i_rst)
    variable v_pending_reads : T_READ_COUNT;
  begin
    if i_rst = '1' then
      s_pending_reads <= 0;
      s_can_pop_writes <= '1';
    elsif rising_edge(i_wb_clk) then
      v_pending_reads := s_pending_reads;

      -- Did we pop a read request from the FIFO?
      if s_fifo_rd_en = '1' and a_fifo_rd_we = '0' then
        v_pending_reads := v_pending_reads + 1;
        s_can_pop_writes <= '0';
      end if;

      -- Did we get a read ACK from the SDRAM?
      if s_pending_reads /= 0 and s_sdram_ack = '1' then
        if s_pending_reads = 1 then
          -- TODO(m): It would be great (TM) if we could assert this signal one cycle earlier?
          s_can_pop_writes <= '1';
        end if;
        v_pending_reads := v_pending_reads - 1;
      end if;

      s_pending_reads <= v_pending_reads;
    end if;
  end process;


  --------------------------------------------------------------------------------------------------
  -- SDRAM controller.
  --------------------------------------------------------------------------------------------------

  sdram_controller_1: entity work.sdram
    generic map (
      G_CLK_FREQ_HZ => CPU_CLK_HZ,
      G_ADDR_WIDTH => C_ADDR_WIDTH,
      G_DATA_WIDTH => C_DATA_WIDTH,
      G_SDRAM_A_WIDTH => SDRAM_ADDR_WIDTH,
      G_SDRAM_DQ_WIDTH => SDRAM_DATA_WIDTH,
      G_SDRAM_BA_WIDTH => SDRAM_BANK_WIDTH,
      G_SDRAM_COL_WIDTH => SDRAM_COL_WIDTH,
      G_SDRAM_ROW_WIDTH => SDRAM_ROW_WIDTH,
      G_CAS_LATENCY => CAS_LATENCY,
      G_T_DESL => T_DESL,
      G_T_MRD => T_MRD,
      G_T_RC => T_RC,
      G_T_RCD => T_RCD,
      G_T_RP => T_RP,
      G_T_DPL => T_DPL,
      G_T_REF => T_REF
    )
    port map (
      i_rst => i_rst,
      i_clk => i_wb_clk,

      -- CPU/Wishbone interface.
      i_adr => s_sdram_adr,
      i_dat_w => s_sdram_dat_w,
      i_we => s_sdram_we,
      i_sel => s_sdram_sel,
      i_req => s_sdram_req,
      o_busy => s_sdram_busy,
      o_ack => s_sdram_ack,
      o_dat => s_sdram_dat,

      -- External SDRAM interface.
      o_sdram_a => o_sdram_a,
      o_sdram_ba => o_sdram_ba,
      io_sdram_dq => io_sdram_dq,
      o_sdram_cke => o_sdram_cke,
      o_sdram_cs_n => o_sdram_cs_n,
      o_sdram_ras_n => o_sdram_ras_n,
      o_sdram_cas_n => o_sdram_cas_n,
      o_sdram_we_n => o_sdram_we_n,
      o_sdram_dqm => o_sdram_dqm
    );
end rtl;

